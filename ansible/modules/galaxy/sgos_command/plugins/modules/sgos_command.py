#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import requests
import re
import sys

DOCUMENTATION = r'''
---
module: sgos_command
short_description: This is a module allowing to run commands on our Symantec Reverse-Proxies and Proxies.
version_added: "1.0.0"
description:
 - run commands on our Symantec Reverse-Proxies and Proxies.
options:
    provider:
        description:
          - The host, port, user and password
        required: true
        type: dict
    sgos:
        description:
          - The version of SGOS to upload
        required: true
        type: str
    cmd:
       description:
         - The command to run on the devices
       require: ture
       type: str

author:
    - Jérôme MATER (@MATERJE)
'''

'''
Ansible module to run commands on our Symantec SGOS devices
'''

from ansible.module_utils.basic import AnsibleModule


def sgos_command(bc_command, provider):

    url = 'https://'+provider['host']+':'+str(provider['port'])+'/Secure/Local/console/install_upload_action/cli_post_setup.txt'
    auth = (provider['user'], provider['password'])
    result = []
    enc_token_url = 'https://'+provider['host']+':'+str(provider['port'])+'/EncToken'
    req_token = requests.get(enc_token_url, auth=auth, verify=False)
    resp_token = req_token
    enc_token = resp_token.content
    headers_token = {
        'X-Bluecoat-Enc-Token':enc_token.rstrip()
    }
    req = requests.post(url, headers=headers_token, auth=auth, files={'file': (None, bc_command)}, verify=False)
    test = req.text

    return test

def run_module():
    module_args = dict(
        provider=dict(type='dict', required=True),
        cmd=dict(type='str', required=True)
    )

    result = dict(
        changed=True,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['original_message'] = sgos_command(module.params['cmd'],module.params['provider'])#,module.params['sgos'])
    result['message'] = 'Test on going'

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
   # if module.params['name'] == 'fail me':
   #     module.fail_json(msg='You requested this to fail', **result)
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()

if __name__ == '__main__':
    main()
