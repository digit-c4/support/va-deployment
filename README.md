# VA deployment
## AWX fail job troubleshoot
**1)** When getting a mail from "snet@ec.europa.eu" that should look like that:

![failed.job image](./docs/img/mail_failed_job.PNG)

**2)** Click on the link on top and you should be directly redirected to the output of the failed job on AWX.
When there you should see something like this:

![failed.job image](./docs/img/failed_job_output.PNG)
This tells you exactly where the issue situated on the policy, in our case on line 3635 becasue of the "FORCE_DENY client.effective_address=!()" that expects a value.

**3)** Next step is to generate the policy so you can troubleshoot it.
To do so in the same window in AWX if you scroll up in the output you shoud see a task called "Import XXX Policy --> Please wait during the import".

![failed.job image](./docs/img/policy_import.PNG)

Click on it and another window should open and search for "url".

![failed.job image](./docs/img/import_url.PNG)

Copy the url in your browser and the policy should generate.
Copy the generation and paste it in prefernce in a Notepad++ to see the line counts.
And go to the line mentioned in the AWX output there is the problem.

In our case te issue is due to a mapping (https://acc-services.unionregistry.ec.europa.eu)

![failed.job image](./docs/img/mapping.PNG)

**4)**To troubleshoot this go to mapui (https://intragate.ec.europa.eu/snet/mapui/main.html).
On the search box at the top left enter the mapping (https://acc-services.unionregistry.ec.europa.eu) and press the search button on the top right.
You should get something like this:

![failed.job image](./docs/img/mapping_2.PNG)

Navigate to "Authentications" tab and you should see some entries.
In our case there are two one in orange and one in white.
The orange one was probably the existing one that has been disabled for someone to test.

![failed.job image](./docs/img/authentications.PNG)

To see who made the latest changes there is a red watch in each entry that you can click.

![failed.job image](./docs/img/watch.PNG)

Here you see that "chavben" has disabled it so you can ask if he's working on it, so you know that someone is on it.

![failed.job image](./docs/img/logs.PNG)

When doing such changes you can generate the policy and compile it on the lab to do so on the bottom left click on "Generation".
A window should open on the right and on the top right there should be a button with a check mark.
Click on it and it will check if the changes are valid or not.

![failed.job image](./docs/img/mapping_policy.PNG)

If there is a "OK" the bottom this means that the changes are working.



## HC workaround
**1)** First go the the follwing AWX:

To use the HC workaround please go on this AWX: https://awx-prod.snmc.cec.eu.int/

**2)** Next go into Templates and search for BC_VA_management

![HC workaround image](./docs/img/HC_1.PNG)

And click on the the rocket on the right:

![HC workaround image](./docs/img/HC_2.PNG)

**3)** Next step questions:
This will prompt you with some test boxes/choice boxes.
1. Here you can enter either a single host name without -mgt (betz-sgpoolint1, wind-sgpoolint2) or enter a group of hosts.
   If you want to enter a group of host you can either choose a whole environment by typing rpsVA_X. X for your environment.

   <details>
      <summary>NEW GROUP OF HOST CREATION</summary>
      If you want to create a custome group of host go into **Inventories** and search for **rps**.
      Click on **rpsVA_HOSTS**

      ![HC workaround image](./docs/img/host_1.PNG)

      Next click on Groups and click on **Add**

      ![HC workaround image](./docs/img/host_2.PNG)

      Now enter the name of your group and click on save.

      Next after saving on the navigation bar click on **Hosts**, click on **Add** and select **Add existing host**

      ![HC workaround image](./docs/img/host_3.PNG)

      Finally search for the needed machine and select it.

      ![HC workaround image](./docs/img/host_4.PNG)
   </details>

2. Than you have to choose the health state for the internal and external.
   Chose healthy for the state to be healthy and choose sick for the state to be unhealthy
   Note that you can leave the choice box empty if you don't want to change either the internal or external.

3. Finally choose your environment.

**4)** Click next at the bottom.

**5)** And Launch.


## Snapshot creation
**1)** First go the the follwing AWX:

To use the snapshot playbook please go on this AWX: https://awx-prod.snmc.cec.eu.int/

**2)** Next go into Templates and search for BC_VA_snapshot

![HC workaround image](./docs/img/snapshot_1.PNG)

And click on the the rocket on the right:

![HC workaround image](./docs/img/HC_2.PNG)

**3)** Next step questions:
This will prompt you with some test boxes/choice boxes.
1. Choose that datacenter to be used WIND-TC for production and labo for lab.

2. Than enter your VA name without -mgt (betz-sgpoolint1, wind-sgpoolint2)

3. Finally select in the multiple choice box on what vcenter the snapshot should go (vcenter-prd.snmc.cec.eu.int for production and vcenter-lab.snmc.cec.eu.int for lab).

**4)** Click next at the bottom.

**5)** And Launch.

## Certificate renewal
**1)** Please renew the certificate on autocert first (https://intragate.ec.europa.eu/snet/wiki/index.php/Service_Support/certificates/Renew_an_existing_certificate).
       The certificate to be renewed are for the management and for the service only.

**2)** Once the certificate is imported a single workflow is to be executed. (https://awx-prod.snmc.cec.eu.int/#/templates/workflow_job_template/574/details)
1. Click on launch

   ![renew workflow](./docs/img/renew_1.PNG)

2. First thing to give is a tag on which environnment the VA is ex. Europa, Stargate, TDM, etc... .Don't forget to click on the proposition under the input field to confirm your choice and click on Next.

   ![renew workflow](./docs/img/renew_2.PNG)

3. Next step you will be asked about the target so ex. betz-sgpoolintXX or wind-sgpoolintXX and click on Next and Launch.

   ![renew workflow](./docs/img/renew_3.PNG)

4. Wait for the playbook to finish and the certificate will be renewed.

   ![renew workflow](./docs/img/renew_4.PNG)
